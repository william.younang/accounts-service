FROM openjdk:11-jre-slim
ADD target/accounts-service-1.0.0.jar /app.jar
RUN touch /app.jar
EXPOSE 8091
ENTRYPOINT ["java", "-jar","/app.jar"]
