package lab.accounts.accountsservice.controller;

import javax.validation.Valid;

import org.springframework.http.HttpHeaders;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import lab.accounts.accountsservice.model.generated.AuthorizationRequest;
import lab.accounts.accountsservice.model.generated.AuthorizationResponse;
import lab.accounts.accountsservice.service.AuthorizationService;
import reactor.core.publisher.Mono;

@RestController
@Validated
public class AuthorizationController {

    private final AuthorizationService authorizationService;

    public AuthorizationController(AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    @PostMapping("/authorization")
    public Mono<AuthorizationResponse> validateTokenAndGetAuthorizationResponse(@RequestHeader HttpHeaders httpHeaders, @RequestBody @Valid AuthorizationRequest authorizationRequest){
        return this.authorizationService.validateTokenAndGetAuthorizationResponse(httpHeaders, authorizationRequest);
    }
}
