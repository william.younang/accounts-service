package lab.accounts.accountsservice.service;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Set;

import lab.accounts.accountsservice.exception.BadRequestException;
import org.keycloak.representations.AccessToken;
import org.keycloak.representations.UserInfo;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lab.accounts.accountsservice.exception.ForbiddenException;
import lab.accounts.accountsservice.exception.InternalServerException;
import lab.accounts.accountsservice.exception.UnauthorizedException;
import lab.accounts.accountsservice.model.generated.AuthorizationRequest;
import lab.accounts.accountsservice.model.generated.AuthorizationResponse;
import reactor.core.publisher.Mono;

@Service
public class AuthorizationService {

    private final WebClient userInfoWebClient;
    private final String clientId;
    private final ObjectMapper objectMapper;
    private final Base64.Decoder decoder;

    public AuthorizationService(@Qualifier("userInfoWebClient") WebClient userInfoWebClient,
    @Value("${keycloak.client-id}") String clientId, ObjectMapper objectMapper) {
        this.userInfoWebClient = userInfoWebClient;
        this.clientId = clientId;
        this.objectMapper = objectMapper;
        this.decoder = Base64.getUrlDecoder();
    }

    public Mono<AuthorizationResponse> validateTokenAndGetAuthorizationResponse(final HttpHeaders httpHeaders, final AuthorizationRequest authorizationRequest){
        return this.userInfoWebClient.get()
                .header(HttpHeaders.AUTHORIZATION, httpHeaders.getFirst(HttpHeaders.AUTHORIZATION))
                .retrieve()
                .onStatus(httpStatus -> httpStatus == HttpStatus.UNAUTHORIZED, error -> Mono.error(new UnauthorizedException()))
                .onStatus(httpStatus -> httpStatus == HttpStatus.BAD_REQUEST, error -> Mono.error(new BadRequestException()))
                .onStatus(httpStatus -> httpStatus == HttpStatus.FORBIDDEN, error -> Mono.error(new ForbiddenException()))
                .onStatus(httpStatus -> httpStatus == HttpStatus.INTERNAL_SERVER_ERROR, error -> Mono.error(new InternalServerException()))
                .bodyToMono(UserInfo.class)
                .map(userInfo -> {

                    final Set<String> bearerTokenRoles = this.getBearerTokenRoles(httpHeaders);

                    if(bearerTokenRoles.stream().noneMatch(authorizationRequest.getRoles()::contains)){
                        throw new ForbiddenException();
                    }

                    return AuthorizationResponse
                            .builder()
                            .fullName(userInfo.getName())
                            .userName(userInfo.getPreferredUsername())
                            .email(userInfo.getEmail())
                            .firstName(userInfo.getGivenName())
                            .lastName(userInfo.getFamilyName())
                            .emailIsVerified(userInfo.getEmailVerified())
                            .roles(new ArrayList<>(bearerTokenRoles))
                            .build();
                });
    }

    private Set<String> getBearerTokenRoles(final HttpHeaders httpHeaders){
        final String token = httpHeaders.getFirst(HttpHeaders.AUTHORIZATION).split(" ")[1];
        final String[] chunks = token.split("\\.");
        final String payload = new String(decoder.decode(chunks[1]));
        try {
            final AccessToken accessToken = objectMapper.readValue(payload, AccessToken.class);
            final AccessToken.Access access = accessToken.getResourceAccess().get(this.clientId);
            return access.getRoles();
        } catch (JsonProcessingException e) {
            throw new InternalServerException();
        }
    }

}
