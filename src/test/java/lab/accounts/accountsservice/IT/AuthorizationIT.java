package lab.accounts.accountsservice.IT;

import java.io.IOException;
import java.util.Arrays;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.keycloak.representations.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import com.fasterxml.jackson.databind.ObjectMapper;

import lab.accounts.accountsservice.model.generated.AuthorizationRequest;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import reactor.core.publisher.Mono;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class AuthorizationIT {

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private WebTestClient webClient;

	public static MockWebServer mockBackEnd;

	@DynamicPropertySource
	static void properties(DynamicPropertyRegistry r) throws IOException {
		r.add("keycloak.user-info-uri", () -> "http://localhost:" + mockBackEnd.getPort());
	}

	@BeforeAll
	static void setUp() throws IOException {
		mockBackEnd = new MockWebServer();
		mockBackEnd.start();
	}

	@AfterAll
	static void tearDown() throws IOException {
		mockBackEnd.shutdown();
	}

	@Test
	void givenAuthorizationRequest_rolesMatch_shouldReturnAuthorizationResponse() throws Exception {
		final var mockUserInfo = new UserInfo();
		mockUserInfo.setEmail("williamyounang@yahoo.fr");
		mockUserInfo.setName("William YOUNANG");
		mockUserInfo.setPreferredUsername("wyounang");
		mockUserInfo.setEmailVerified(Boolean.TRUE);
		mockUserInfo.setGivenName("William");
		mockUserInfo.setFamilyName("YOUNANG");

		final String token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJjUnlRRXhlMWR4eU1TbDk4ZE1wSVktdDBRWHpGSkRqWllFbnhlMjM4b0owIn0.eyJleHAiOjE2NDEzMTM1MDYsImlhdCI6MTY0MTMxMjkwNiwianRpIjoiYWNlM2ZiOTEtMDEyMS00ZTQ3LWFjZDEtYzY0MmZhMjBmNTM1IiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwL2F1dGgvcmVhbG1zL29wZW5oIiwiYXVkIjpbInJlYWxtLW1hbmFnZW1lbnQiLCJhY2NvdW50Il0sInN1YiI6IjFjMmNhYjczLTkxOGUtNDczNi05NjI0LWJkMmExMDM1YmQ2YSIsInR5cCI6IkJlYXJlciIsImF6cCI6Imh1bWFuLWNsaW5pYyIsInNlc3Npb25fc3RhdGUiOiJkMWUyMTc1MS05Yzg1LTRlMzktOTQ0MS1mMDdjNGIzYjFjMjciLCJhY3IiOiIxIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iLCJkZWZhdWx0LXJvbGVzLW9wZW5oIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsicmVhbG0tbWFuYWdlbWVudCI6eyJyb2xlcyI6WyJtYW5hZ2UtdXNlcnMiXX0sImh1bWFuLWNsaW5pYyI6eyJyb2xlcyI6WyJSRUFEX1BBVElFTlQiLCJDUkVBVEVfUEFUSUVOVCIsIkRFTEVURV9QQVRJRU5UIiwibWFuYWdlLXVzZXJzIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJzaWQiOiJkMWUyMTc1MS05Yzg1LTRlMzktOTQ0MS1mMDdjNGIzYjFjMjciLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwibmFtZSI6IldpbGxpYW0gWU9VTkFORyIsInByZWZlcnJlZF91c2VybmFtZSI6Ind5b3VuYW5nIiwiZ2l2ZW5fbmFtZSI6IldpbGxpYW0iLCJmYW1pbHlfbmFtZSI6IllPVU5BTkciLCJlbWFpbCI6IndpbGxpYW15b3VuYW5nQHlhaG9vLmZyIn0.d1uzjCOBMxJdNVYPlLiU7dKIY8tSDjN-8IWyof8cfbenEknXRLmrT-ujBem0NH3eP1QWzTQPjaDZy7UNSRqVQw0Nd8yh5RAxRhlC4WbbvdKiNi8xY-AbnFPEhshBrz8wqFRUDKyT9i7Aypsg4XTJzc4uWbl1_wA7lPQKTOcgyuf3qoeJA_CzhDTf3To2hWGFpOy8NMiLs3MimKcePImeQE6IfO7YP4yDYpv2MpMEVIc7XIS2z6rr2YkmqlfTder7A8PLdJj5HnzJDef9Mkyr9lUozKI3_OKAw9rG_yHg3QnU0RH7isRhUxtrE9QCIx8JiL_YFiS3wz0qADjcZAAfJg";
		final AuthorizationRequest authorizationRequest = AuthorizationRequest.builder().roles(Arrays.asList("CREATE_ACCOUNT", "DELETE_ACCOUNT", "READ_PATIENT")).build();


		mockBackEnd.enqueue(new MockResponse()
				.setBody(objectMapper.writeValueAsString(mockUserInfo))
				.addHeader("Content-Type", "application/json"));

		webClient.post()
				.uri("/authorization")
				.contentType(MediaType.APPLICATION_JSON)
				.header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
				.body(BodyInserters.fromPublisher(Mono.just(authorizationRequest), AuthorizationRequest.class))
				.exchange()
				.expectStatus().isOk()
				.expectBody()
				.jsonPath("$.userName").isEqualTo("wyounang")
				.jsonPath("$.firstName").isEqualTo("William")
				.jsonPath("$.lastName").isEqualTo("YOUNANG")
				.jsonPath("$.fullName").isEqualTo("William YOUNANG")
				.jsonPath("$.email").isEqualTo("williamyounang@yahoo.fr")
				.jsonPath("$.roles").isArray()
				.jsonPath("$.emailIsVerified").isEqualTo("true");
	}

	@Test
	void givenAuthorizationRequest_rolesDontMatch_shouldThrowForbidden() throws Exception {
		final var mockUserInfo = new UserInfo();
		mockUserInfo.setEmail("williamyounang@yahoo.fr");
		mockUserInfo.setName("William YOUNANG");
		mockUserInfo.setPreferredUsername("wyounang");
		mockUserInfo.setEmailVerified(Boolean.TRUE);
		mockUserInfo.setGivenName("William");
		mockUserInfo.setFamilyName("YOUNANG");

		final String token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJjUnlRRXhlMWR4eU1TbDk4ZE1wSVktdDBRWHpGSkRqWllFbnhlMjM4b0owIn0.eyJleHAiOjE2NDEzMTM1MDYsImlhdCI6MTY0MTMxMjkwNiwianRpIjoiYWNlM2ZiOTEtMDEyMS00ZTQ3LWFjZDEtYzY0MmZhMjBmNTM1IiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwL2F1dGgvcmVhbG1zL29wZW5oIiwiYXVkIjpbInJlYWxtLW1hbmFnZW1lbnQiLCJhY2NvdW50Il0sInN1YiI6IjFjMmNhYjczLTkxOGUtNDczNi05NjI0LWJkMmExMDM1YmQ2YSIsInR5cCI6IkJlYXJlciIsImF6cCI6Imh1bWFuLWNsaW5pYyIsInNlc3Npb25fc3RhdGUiOiJkMWUyMTc1MS05Yzg1LTRlMzktOTQ0MS1mMDdjNGIzYjFjMjciLCJhY3IiOiIxIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iLCJkZWZhdWx0LXJvbGVzLW9wZW5oIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsicmVhbG0tbWFuYWdlbWVudCI6eyJyb2xlcyI6WyJtYW5hZ2UtdXNlcnMiXX0sImh1bWFuLWNsaW5pYyI6eyJyb2xlcyI6WyJSRUFEX1BBVElFTlQiLCJDUkVBVEVfUEFUSUVOVCIsIkRFTEVURV9QQVRJRU5UIiwibWFuYWdlLXVzZXJzIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJzaWQiOiJkMWUyMTc1MS05Yzg1LTRlMzktOTQ0MS1mMDdjNGIzYjFjMjciLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwibmFtZSI6IldpbGxpYW0gWU9VTkFORyIsInByZWZlcnJlZF91c2VybmFtZSI6Ind5b3VuYW5nIiwiZ2l2ZW5fbmFtZSI6IldpbGxpYW0iLCJmYW1pbHlfbmFtZSI6IllPVU5BTkciLCJlbWFpbCI6IndpbGxpYW15b3VuYW5nQHlhaG9vLmZyIn0.d1uzjCOBMxJdNVYPlLiU7dKIY8tSDjN-8IWyof8cfbenEknXRLmrT-ujBem0NH3eP1QWzTQPjaDZy7UNSRqVQw0Nd8yh5RAxRhlC4WbbvdKiNi8xY-AbnFPEhshBrz8wqFRUDKyT9i7Aypsg4XTJzc4uWbl1_wA7lPQKTOcgyuf3qoeJA_CzhDTf3To2hWGFpOy8NMiLs3MimKcePImeQE6IfO7YP4yDYpv2MpMEVIc7XIS2z6rr2YkmqlfTder7A8PLdJj5HnzJDef9Mkyr9lUozKI3_OKAw9rG_yHg3QnU0RH7isRhUxtrE9QCIx8JiL_YFiS3wz0qADjcZAAfJg";
		final AuthorizationRequest authorizationRequest = AuthorizationRequest.builder().roles(Arrays.asList("CREATE_ACCOUNT", "DELETE_ACCOUNT")).build();

		mockBackEnd.enqueue(new MockResponse()
				.setBody(objectMapper.writeValueAsString(mockUserInfo))
				.addHeader("Content-Type", "application/json"));

		webClient.post()
				.uri("/authorization")
				.contentType(MediaType.APPLICATION_JSON)
				.header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
				.body(BodyInserters.fromPublisher(Mono.just(authorizationRequest), AuthorizationRequest.class))
				.exchange()
				.expectStatus().isForbidden();
	}


	@Test
	void givenAuthorizationRequest_authorizationHeaderIsMissed_shouldThrowBadRequest() {

		final AuthorizationRequest authorizationRequest = AuthorizationRequest.builder().roles(Arrays.asList("CREATE_ACCOUNT", "DELETE_ACCOUNT")).build();

		mockBackEnd.enqueue(new MockResponse()
				.setResponseCode(400)
				.addHeader("Content-Type", "application/json"));

		webClient.post()
				.uri("/authorization")
				.contentType(MediaType.APPLICATION_JSON)
				.body(BodyInserters.fromPublisher(Mono.just(authorizationRequest), AuthorizationRequest.class))
				.exchange()
				.expectStatus().isBadRequest();
	}

	@Test
	void givenAuthorizationRequest_accessTokenIsExpired_shouldThrowUnauthorized() {

		final AuthorizationRequest authorizationRequest = AuthorizationRequest.builder().roles(Arrays.asList("CREATE_ACCOUNT", "DELETE_ACCOUNT")).build();
		final String token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJjUnlRRXhlMWR4eU1TbDk4ZE1wSVktdDBRWHpGSkRqWllFbnhlMjM4b0owIn0.eyJleHAiOjE2NDEzMTM1MDYsImlhdCI6MTY0MTMxMjkwNiwianRpIjoiYWNlM2ZiOTEtMDEyMS00ZTQ3LWFjZDEtYzY0MmZhMjBmNTM1IiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwL2F1dGgvcmVhbG1zL29wZW5oIiwiYXVkIjpbInJlYWxtLW1hbmFnZW1lbnQiLCJhY2NvdW50Il0sInN1YiI6IjFjMmNhYjczLTkxOGUtNDczNi05NjI0LWJkMmExMDM1YmQ2YSIsInR5cCI6IkJlYXJlciIsImF6cCI6Imh1bWFuLWNsaW5pYyIsInNlc3Npb25fc3RhdGUiOiJkMWUyMTc1MS05Yzg1LTRlMzktOTQ0MS1mMDdjNGIzYjFjMjciLCJhY3IiOiIxIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iLCJkZWZhdWx0LXJvbGVzLW9wZW5oIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsicmVhbG0tbWFuYWdlbWVudCI6eyJyb2xlcyI6WyJtYW5hZ2UtdXNlcnMiXX0sImh1bWFuLWNsaW5pYyI6eyJyb2xlcyI6WyJSRUFEX1BBVElFTlQiLCJDUkVBVEVfUEFUSUVOVCIsIkRFTEVURV9QQVRJRU5UIiwibWFuYWdlLXVzZXJzIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJzaWQiOiJkMWUyMTc1MS05Yzg1LTRlMzktOTQ0MS1mMDdjNGIzYjFjMjciLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwibmFtZSI6IldpbGxpYW0gWU9VTkFORyIsInByZWZlcnJlZF91c2VybmFtZSI6Ind5b3VuYW5nIiwiZ2l2ZW5fbmFtZSI6IldpbGxpYW0iLCJmYW1pbHlfbmFtZSI6IllPVU5BTkciLCJlbWFpbCI6IndpbGxpYW15b3VuYW5nQHlhaG9vLmZyIn0.d1uzjCOBMxJdNVYPlLiU7dKIY8tSDjN-8IWyof8cfbenEknXRLmrT-ujBem0NH3eP1QWzTQPjaDZy7UNSRqVQw0Nd8yh5RAxRhlC4WbbvdKiNi8xY-AbnFPEhshBrz8wqFRUDKyT9i7Aypsg4XTJzc4uWbl1_wA7lPQKTOcgyuf3qoeJA_CzhDTf3To2hWGFpOy8NMiLs3MimKcePImeQE6IfO7YP4yDYpv2MpMEVIc7XIS2z6rr2YkmqlfTder7A8PLdJj5HnzJDef9Mkyr9lUozKI3_OKAw9rG_yHg3QnU0RH7isRhUxtrE9QCIx8JiL_YFiS3wz0qADjcZAAfJg";

		mockBackEnd.enqueue(new MockResponse()
				.setResponseCode(401)
				.addHeader("Content-Type", "application/json"));

		webClient.post()
				.uri("/authorization")
				.contentType(MediaType.APPLICATION_JSON)
				.header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
				.body(BodyInserters.fromPublisher(Mono.just(authorizationRequest), AuthorizationRequest.class))
				.exchange()
				.expectStatus().isUnauthorized();
	}

	@Test
	void givenAuthorizationRequest_rolesAreMissedFromAuthorizationRequest_shouldThrowBadRequest() {

		final AuthorizationRequest authorizationRequest = AuthorizationRequest.builder().build();
		final String token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJjUnlRRXhlMWR4eU1TbDk4ZE1wSVktdDBRWHpGSkRqWllFbnhlMjM4b0owIn0.eyJleHAiOjE2NDEzMTM1MDYsImlhdCI6MTY0MTMxMjkwNiwianRpIjoiYWNlM2ZiOTEtMDEyMS00ZTQ3LWFjZDEtYzY0MmZhMjBmNTM1IiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwL2F1dGgvcmVhbG1zL29wZW5oIiwiYXVkIjpbInJlYWxtLW1hbmFnZW1lbnQiLCJhY2NvdW50Il0sInN1YiI6IjFjMmNhYjczLTkxOGUtNDczNi05NjI0LWJkMmExMDM1YmQ2YSIsInR5cCI6IkJlYXJlciIsImF6cCI6Imh1bWFuLWNsaW5pYyIsInNlc3Npb25fc3RhdGUiOiJkMWUyMTc1MS05Yzg1LTRlMzktOTQ0MS1mMDdjNGIzYjFjMjciLCJhY3IiOiIxIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iLCJkZWZhdWx0LXJvbGVzLW9wZW5oIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsicmVhbG0tbWFuYWdlbWVudCI6eyJyb2xlcyI6WyJtYW5hZ2UtdXNlcnMiXX0sImh1bWFuLWNsaW5pYyI6eyJyb2xlcyI6WyJSRUFEX1BBVElFTlQiLCJDUkVBVEVfUEFUSUVOVCIsIkRFTEVURV9QQVRJRU5UIiwibWFuYWdlLXVzZXJzIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJzaWQiOiJkMWUyMTc1MS05Yzg1LTRlMzktOTQ0MS1mMDdjNGIzYjFjMjciLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwibmFtZSI6IldpbGxpYW0gWU9VTkFORyIsInByZWZlcnJlZF91c2VybmFtZSI6Ind5b3VuYW5nIiwiZ2l2ZW5fbmFtZSI6IldpbGxpYW0iLCJmYW1pbHlfbmFtZSI6IllPVU5BTkciLCJlbWFpbCI6IndpbGxpYW15b3VuYW5nQHlhaG9vLmZyIn0.d1uzjCOBMxJdNVYPlLiU7dKIY8tSDjN-8IWyof8cfbenEknXRLmrT-ujBem0NH3eP1QWzTQPjaDZy7UNSRqVQw0Nd8yh5RAxRhlC4WbbvdKiNi8xY-AbnFPEhshBrz8wqFRUDKyT9i7Aypsg4XTJzc4uWbl1_wA7lPQKTOcgyuf3qoeJA_CzhDTf3To2hWGFpOy8NMiLs3MimKcePImeQE6IfO7YP4yDYpv2MpMEVIc7XIS2z6rr2YkmqlfTder7A8PLdJj5HnzJDef9Mkyr9lUozKI3_OKAw9rG_yHg3QnU0RH7isRhUxtrE9QCIx8JiL_YFiS3wz0qADjcZAAfJg";

		webClient.post()
				.uri("/authorization")
				.contentType(MediaType.APPLICATION_JSON)
				.header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
				.body(BodyInserters.fromPublisher(Mono.just(authorizationRequest), AuthorizationRequest.class))
				.exchange()
				.expectStatus().isBadRequest();
	}

}
