package lab.accounts.accountsservice.controller;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;

import lab.accounts.accountsservice.model.generated.AuthorizationRequest;
import lab.accounts.accountsservice.model.generated.AuthorizationResponse;
import lab.accounts.accountsservice.service.AuthorizationService;
import reactor.core.publisher.Mono;

@ExtendWith(MockitoExtension.class)
class AuthorizationControllerTest {

	@Mock
	private AuthorizationService authorizationService;

	@InjectMocks
	private AuthorizationController authorizationController;

	@Test
	void validateTokenAndGetAuthorizationResponse_shouldReturnAuthorizationResponse(){
		final HttpHeaders httpHeaders = new HttpHeaders();
		final String token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJjUnlRRXhlMWR4eU1TbDk4ZE1wSVktdDBRWHpGSkRqWllFbnhlMjM4b0owIn0.eyJleHAiOjE2NDEzMTM1MDYsImlhdCI6MTY0MTMxMjkwNiwianRpIjoiYWNlM2ZiOTEtMDEyMS00ZTQ3LWFjZDEtYzY0MmZhMjBmNTM1IiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwL2F1dGgvcmVhbG1zL29wZW5oIiwiYXVkIjpbInJlYWxtLW1hbmFnZW1lbnQiLCJhY2NvdW50Il0sInN1YiI6IjFjMmNhYjczLTkxOGUtNDczNi05NjI0LWJkMmExMDM1YmQ2YSIsInR5cCI6IkJlYXJlciIsImF6cCI6Imh1bWFuLWNsaW5pYyIsInNlc3Npb25fc3RhdGUiOiJkMWUyMTc1MS05Yzg1LTRlMzktOTQ0MS1mMDdjNGIzYjFjMjciLCJhY3IiOiIxIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iLCJkZWZhdWx0LXJvbGVzLW9wZW5oIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsicmVhbG0tbWFuYWdlbWVudCI6eyJyb2xlcyI6WyJtYW5hZ2UtdXNlcnMiXX0sImh1bWFuLWNsaW5pYyI6eyJyb2xlcyI6WyJSRUFEX1BBVElFTlQiLCJDUkVBVEVfUEFUSUVOVCIsIkRFTEVURV9QQVRJRU5UIiwibWFuYWdlLXVzZXJzIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJzaWQiOiJkMWUyMTc1MS05Yzg1LTRlMzktOTQ0MS1mMDdjNGIzYjFjMjciLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwibmFtZSI6IldpbGxpYW0gWU9VTkFORyIsInByZWZlcnJlZF91c2VybmFtZSI6Ind5b3VuYW5nIiwiZ2l2ZW5fbmFtZSI6IldpbGxpYW0iLCJmYW1pbHlfbmFtZSI6IllPVU5BTkciLCJlbWFpbCI6IndpbGxpYW15b3VuYW5nQHlhaG9vLmZyIn0.d1uzjCOBMxJdNVYPlLiU7dKIY8tSDjN-8IWyof8cfbenEknXRLmrT-ujBem0NH3eP1QWzTQPjaDZy7UNSRqVQw0Nd8yh5RAxRhlC4WbbvdKiNi8xY-AbnFPEhshBrz8wqFRUDKyT9i7Aypsg4XTJzc4uWbl1_wA7lPQKTOcgyuf3qoeJA_CzhDTf3To2hWGFpOy8NMiLs3MimKcePImeQE6IfO7YP4yDYpv2MpMEVIc7XIS2z6rr2YkmqlfTder7A8PLdJj5HnzJDef9Mkyr9lUozKI3_OKAw9rG_yHg3QnU0RH7isRhUxtrE9QCIx8JiL_YFiS3wz0qADjcZAAfJg";
		httpHeaders.add(HttpHeaders.AUTHORIZATION, "Bearer " + token);
		final AuthorizationRequest authorizationRequest = AuthorizationRequest.builder().roles(Arrays.asList("CREATE_ACCOUNT", "DELETE_ACCOUNT", "READ_PATIENT")).build();

		Mockito.when(authorizationService.validateTokenAndGetAuthorizationResponse(httpHeaders, authorizationRequest))
				.thenReturn(Mono.just(AuthorizationResponse
						.builder()
						.fullName("William YOUNANG")
						.email("williamyounang@yahoo.fr")
						.emailIsVerified(true)
						.firstName("William")
						.lastName("YOUNANG")
						.userName("wyounang")
						.roles(Arrays.asList("READ_PATIENT", "CREATE_PATIENT", "DELETE_PATIENT", "manage-users"))
						.build()));

		final Mono<AuthorizationResponse> authorizationResponseMono = authorizationController.validateTokenAndGetAuthorizationResponse(httpHeaders, authorizationRequest);

		assertTrue(authorizationResponseMono.block().getEmailIsVerified());
	}


}